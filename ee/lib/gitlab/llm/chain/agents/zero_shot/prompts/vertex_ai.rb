# frozen_string_literal: true

module Gitlab
  module Llm
    module Chain
      module Agents
        module ZeroShot
          module Prompts
            class VertexAi < Base
            end
          end
        end
      end
    end
  end
end
